import Vue from 'vue'
import VueRouter from 'vue-router'

/* Layouts */
import AppLayout from '@/layouts/AppLayout'

/* Views */
import Landing from '@/views/Landing'
import Home from '@/views/Home'

import Packages from '@/views/Packages'
import Package from '@/views/Package'
import PackageDetails from '@/views/Package/Details'
import PackageReleases from '@/views/Package/Releases'
import PackageMedia from '@/views/Package/Media'
import PackageComments from '@/views/Package/Comments'
import PackageRelationships from '@/views/Package/Relationships'

import Communities from '@/views/Communities'
import Community from '@/views/Community'
import CommunityHome from '@/views/Community/Home'
import CommunityPackages from '@/views/Community/Packages'
import CommunityNews from '@/views/Community/News'
import CommunityMedia from '@/views/Community/Media'

import Games from '@/views/Games'
import Docs from '@/views/Docs'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: {
      render: (h) => {
        // eslint-disable-next-line no-constant-condition
        if (false) { // TODO: Do logged in state check here
          return h(Home)
        } else {
          return h(Landing)
        }
      }
    },
    meta: { layout: AppLayout }
  },
  {
    path: '/packages',
    name: 'Packages',
    component: Packages,
    meta: { layout: AppLayout }
  },
  {
    path: '/packages/:package',
    name: 'Package',
    component: Package,
    meta: { layout: AppLayout },
    children: [
      {
        path: '',
        component: PackageDetails,
        meta: { layout: AppLayout }
      },
      {
        path: 'details',
        component: PackageDetails,
        meta: { layout: AppLayout }
      },
      {
        path: 'releases',
        component: PackageReleases,
        meta: { layout: AppLayout }
      },
      {
        path: 'media',
        component: PackageMedia,
        meta: { layout: AppLayout }
      },
      {
        path: 'comments',
        component: PackageComments,
        meta: { layout: AppLayout }
      },
      {
        path: 'relationships',
        component: PackageRelationships,
        meta: { layout: AppLayout }
      }
    ]
  },
  {
    path: '/communities',
    name: 'Communities',
    component: Communities,
    meta: { layout: AppLayout }
  },
  {
    path: '/communities/:community',
    name: 'Community',
    component: Community,
    meta: { layout: AppLayout },
    children: [
      {
        path: '',
        component: CommunityHome,
        meta: { layout: AppLayout }
      },
      {
        path: 'packages',
        component: CommunityPackages,
        meta: { layout: AppLayout }
      },
      {
        path: 'news',
        component: CommunityNews,
        meta: { layout: AppLayout }
      },
      {
        path: 'media',
        component: CommunityMedia,
        meta: { layout: AppLayout }
      }
    ]
  },
  {
    path: '/games',
    name: 'Games',
    component: Games,
    meta: { layout: AppLayout }
  },
  {
    path: '/docs',
    name: 'Docs',
    component: Docs,
    meta: { layout: AppLayout }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
